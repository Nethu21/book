package book;

public class des {
	
		private double width;
		private double height;
		
		public Rectangle(double width, double height) {
			this.width = width;
			this.height = height;
		}
		
		public double getWidth() {
			return width;
		}
		
		public void setWidth(double width) {
			this.width = width;
		}
		public double getHeight() {
			return height;
		}
		public void setHeight(double height) {
			this.height = height;
		}
		public double getArea() {
			return width*height;
		}
		public double getDiagonalLength() {
			return Math.sqrt(width*width+height*height);
		}
		
		public static void main(String[] args) {
			Rectangle o1 = new Rectangle(23,6);
			double w = o1.getWidth();
			System.out.println(w);
		}
	}

